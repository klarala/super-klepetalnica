/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function Slike(besedilo) {
  var neki = "";
  besedilo = besedilo.split(" ");
  for (var i = 0; i < besedilo.length; i++) {
    if ((besedilo[i].indexOf("http") == 0) && besedilo[i].indexOf(".jpg" || ".png" || ".gif") > -1) {
      neki = neki + "<br /><img style='width:200px; margin-left:20px' src= '"+ besedilo[i] + "' />";
    }
  }
  besedilo = besedilo.join(" ") + neki;
  return besedilo;
}

var uporabniki2 = [];
var nadimki = [];

/*global Nadimek*/
function Nadimek(uporabnik, nadimek) {
  for(var i in uporabniki2){
    if(uporabniki2[i] == uporabnik){
      nadimki[i] = nadimek;
    }
  }
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  sporocilo = sporocilo.replace(/\'/g,"\"" );
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var krcanje = sporocilo.indexOf("&#9756;") > -1 && sporocilo.indexOf("zasebno") > -1;
  var jeSlika = sporocilo.indexOf("<img style=\"width:200px; margin-left:20px\" src= \"") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if (krcanje) {
      return divElementHtmlTekst(sporocilo);
  }
  if (jeSlika) {
    return divElementHtmlTekst(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = Slike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var vhod = sporocilo.besedilo.split(":");
    vhod[0] = vhod[0].substring(0,vhod[0].length);
    var a = -1;
    for(var i in uporabniki2){
      if (uporabniki2[i] == vhod[0] && uporabniki2[i] != ""){
        a = i;
      }
    }
    if(a == -1){
        var novElement = divElementEnostavniTekst(sporocilo.besedilo);
        $('#sporocila').append(novElement);
    }
    else{
      $('#sporocila').append(divElementEnostavniTekst(nadimki[a] + " (" + uporabniki2[a] + ")" + sporocilo.besedilo.substring(vhod[0].length,sporocilo.besedilo.length)));
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for (var i = 0; i < uporabniki.length; i++) {
      var a = 0;
      for(var j in uporabniki2){
        if(uporabniki2[j] == uporabniki[i]) {
          a = 1;
        }
      }
      if(a == 0){
        uporabniki2.push(uporabniki[i]);
        nadimki.push("");
      }
      var b = -1;
      for (var k in uporabniki2) {
        if (uporabniki2[k] == uporabniki[i] && nadimki[k] != "") {
          b = j;
        }
      }
      if (b == -1) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
      else {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[b] + " (" + uporabniki2[b] + ")"));
      }
    }
    $('#seznam-uporabnikov div').click(function() {
      sistemskoSporocilo = klepetApp.procesirajUkaz("/zasebno \"" + $(this).text() + "\" \"&#9756;\"");
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('preimenovanje', function(blabla) {
    var neki = blabla.besedilo.split(" ");
      for (var i in uporabniki2){
        if (uporabniki2[i] == neki[0]){
          uporabniki2[i] = vhod[1];
        }
      }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
